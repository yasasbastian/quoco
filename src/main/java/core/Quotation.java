package core;

import java.io.Serializable;

/**
 * Interface to define the data to be stored in Quotation objects.
 * 
 * @author Rem
 *
 */
public class Quotation implements Serializable {
	public Quotation(String reference, ClientInfo clientInfo, double price) {
		this.reference = reference;
		this.clientInfo = clientInfo;
		this.price = price;
		
	}
	public String reference;
	public ClientInfo clientInfo;
	public double price;

	@Override
	public boolean equals(Object obj) {
		Quotation quotation = (Quotation) obj;
		return reference.equals(quotation.reference)
				&& clientInfo.equals(quotation.clientInfo)
				&& price == quotation.price;
	}
}
