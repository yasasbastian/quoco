package akka;

import core.ClientInfo;
import core.Quotation;
import quotation.QuotationService;

public interface Messages {
    class Init {
        public QuotationService service;
        public Init(QuotationService service) {
            this.service = service;
        }
    }

    class RequestQuotation {
        public int sequenceNumber;
        public ClientInfo info;
        public RequestQuotation(int sequenceNumber, ClientInfo info) {
            this.sequenceNumber = sequenceNumber;
            this.info = info;
        }
    }

    class Offer {
        public int sequenceNumber;
        public Quotation quotation;
        public Offer(int sequenceNumber, Quotation quotation) {
            this.sequenceNumber = sequenceNumber;
            this.quotation= quotation;
        }

        @Override
        public boolean equals(Object obj) {
            Offer offer = (Offer) obj;
            return (offer.sequenceNumber == sequenceNumber)
                    && offer.quotation.equals(quotation);
        }
    }

    class NoOffer {
        public int sequenceNumber;
        public NoOffer(int sequenceNumber) {
            this.sequenceNumber = sequenceNumber;
        }
    }


}
