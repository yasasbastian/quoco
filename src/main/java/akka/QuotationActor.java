package akka;

import akka.actor.AbstractActor;
import akka.actor.Props;
import akka.stream.impl.QueueSource;
import core.ClientInfo;
import core.Quotation;
import quotation.QuotationService;

public class QuotationActor extends AbstractActor {
    private QuotationService service;
    public static Props createProps() {
        return Props.create(QuotationActor.class);
    }

    public Receive createReceive() {
        return receiveBuilder()
                .match(Messages.Init.class, msg -> {
                    service = msg.service;
                })
                .match(Messages.RequestQuotation.class, msg -> {
                    Quotation quotation = service.generateQuotation(msg.info);
                    getSender().tell(new Messages.Offer(msg.sequenceNumber, quotation), getSelf());
                }).build();

    }


}
